# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, NodeManager
import math

##################################################

# threshold to recognize when there is no light on the refl PD
imc_refl_no_light_threshold = 10

# gain in db for IMC servo during acquisition
imc_servo_acquire_in1gain = -13#-26

##################################################

# check for some error conditions
def in_fault():
    # currently these faults are only checked in acquire and in the
    # fault state, if the MC is not already locked.  This is intended
    # to help a user diagnose problems if the MC is not locking, but
    # not to interefere with the MC lock needlessly by taking it out
    # of the locked state when it is still locked
    message = []
    flag = False

    # a hierarchy of conditions to check if there is light on the refl
    # PD (only give user most relevant fault message)
    if ezca['PSL-PMC_LOCK_ON'] != -30000:
        message.append("PMC unlocked")
        flag = True
    #elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 1:
    elif ezca['IMC-PWR_IN_OUTPUT'] < (ezca['PSL-POWER_REQUEST']-1):
        message.append("PSL periscope PD error (low light?)")
        flag = True
    elif ezca['IMC-REFL_DC_OUTPUT'] < imc_refl_no_light_threshold:
        message.append("PSL REFL PD no light (check PZT/MC1 alignment)")
        flag = True

    # check if FSS is locked
    if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
        message.append("FSS unlocked")
        flag = True

    # check HAM 2/3 ISI watchdogs
    # FIXME: this should look at ISI guardian nodes
    if ezca['ISI-HAM2_WD_MON_STATE_INMON'] != 1:
        message.append("HAM2 ISI tripped")
        flag = True
    if ezca['ISI-HAM3_WD_MON_STATE_INMON'] != 1:
        message.append("HAM3 ISI tripped")
        flag = True

    # check for MC sus guardian alignment states
    for sus in ['MC1', 'MC2', 'MC3']:
        if nodes['SUS_'+sus] != 'ALIGNED':
            message.append(sus+" not aligned")
            flag = True

    # if we get a flag, notify the user, otherwise clear any
    # notifications
    if flag:
        notify(', '.join(message))
    else:
        notify()

    return flag

# check for lock
def is_locked():
    # if the front-end lock trigger is 1 then we're locked
    #return ezca['IMC-MCL_FM_TRIG_MON'] == 1
    # FIXME: we have to look at the trans sum directly, since the
    # trigger is actually shut off after lock is acquired.  Is that
    # necessary?  If not, we can just continue to look at the trigger
    # state (above)
    return ezca['IMC-MC2_TRANS_SUM_INMON'] >= 25*ezca['IMC-PWR_IN_OUTPUT']

##################################################

nodes = NodeManager(['SUS_MC1', 'SUS_MC2', 'SUS_MC3'])

##################################################

# initial request on initialization
request = 'LOCKED'
# nominal operating state
nominal = 'LOCKED'

# determine lock state on initialization
class INIT(GuardState):
    request = True

    def main(self):

        nodes.set_managed()

        if is_locked():
            return 'LOCKED'
        else:
            return 'DOWN'

# wait for all faults to be cleared, the jump to init to find state
class FAULT(GuardState):
    redirect = False
    request = False

    def run(self):
        if in_fault():
            return
        return 'INIT'

# reset everything
class DOWN(GuardState):
    goto = True

    def main(self):
        
        # self.timer['conf'] = 5

        # request suspensions aligned
        for sus in ['MC1', 'MC2', 'MC3']:
            nodes['SUS_'+sus] = 'ALIGNED'

        # # turn off the ISS second loop and reacquire at the current power level later in this script
        ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0
 #       ezca['PSL-ISS_SECONDLOOP_GAIN'] = 40

        # FIXME: this is redundant with some of below
        ezca['IMC-L_GAIN'] = 0
        ezca['IMC-REFL_SERVO_FASTEN'] = 'Off'
        #ezca['IMC-MCL_MASK_FM2'] = 1
        #ezca.switch('IMC-MC2_TRANS_SUM', 'FM1', 'ON')

        # turn off feedback
        ezca['IMC-L_GAIN'] = 0
        ezca['IMC-REFL_SERVO_FASTEN'] = 'Off'

        # turn off MC2 length feedback
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = 0
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0
        ezca.switch('SUS-MC2_M2_LOCK_L', 'FM3', 'OFF')
        ezca.switch('SUS-MC2_M1_LOCK_L', 'INPUT', 'OFF')
        # clear M1 length lock history
        ezca['SUS-MC2_M1_LOCK_L_RSET'] = 2
        
        # turn off WFS and clear history
        # ezca['IMC-WFS_SWTCH'] = 'OFF'
        ezca['IMC-DOF_1_P_RSET'] = 2
        ezca['IMC-DOF_2_P_RSET'] = 2
        ezca['IMC-DOF_3_P_RSET'] = 2
        ezca['IMC-DOF_4_P_RSET'] = 2
        ezca['IMC-DOF_1_Y_RSET'] = 2
        ezca['IMC-DOF_2_Y_RSET'] = 2
        ezca['IMC-DOF_3_Y_RSET'] = 2
        ezca['IMC-DOF_4_Y_RSET'] = 2

        # Make sure WFS whitening is only stage 1
        #system("perl /opt/rtcds/userapps/release/cds/common/scripts/beckhoff_gang_whiten


        # Turn off WFS_DC centering loops (DOF3)
        ezca.switch('IMC-DOF_3_P', 'INPUT', 'OFF')
        ezca.switch('IMC-DOF_3_Y', 'INPUT', 'OFF')

        # Turn off WFS loops and MC2-trans loop
        ezca.switch('IMC-DOF_1_P', 'INPUT', 'OFF')
        ezca.switch('IMC-DOF_1_Y', 'INPUT', 'OFF')
        ezca.switch('IMC-DOF_2_P', 'INPUT', 'OFF')
        ezca.switch('IMC-DOF_2_Y', 'INPUT', 'OFF')
        ezca.switch('IMC-DOF_4_P', 'INPUT', 'OFF')
        ezca.switch('IMC-DOF_4_Y', 'INPUT', 'OFF')

        # Turn off PZT resonance damping loops (DOF5)
        ezca.switch('IMC-DOF_5_P', 'INPUT', 'OFF')
        # ezca.switch('IMC-DOF_5_Y', 'INPUT', 'OFF') # not used, uncomment if needed

        # reset IMC "common mode" servo settings
        ezca['IMC-REFL_SERVO_COMCOMP'] = 'On'
        ezca['IMC-REFL_SERVO_COMFILTER'] = 'Off'
        ezca['IMC-REFL_SERVO_COMBOOST'] = 0
        ezca['IMC-REFL_SERVO_IN1POL'] = 0
        ezca['IMC-REFL_SERVO_FASTPOL'] = 0
        ezca['IMC-REFL_SERVO_IN1GAIN'] = imc_servo_acquire_in1gain
        ezca['IMC-REFL_SERVO_FASTGAIN'] = 0
#        ezca['IMC-REFL_SERVO_FASTGAIN'] = -28 # accomodates extra gain of temporary VCO (4 MHz/V)
        ezca['IMC-REFL_SERVO_IN1EN'] = 'On'
        ezca['IMC-REFL_SERVO_IN2EN'] = 'Off'
        ezca['IMC-REFL_SERVO_SLOWBYPASS'] = 'On'

        # reset IMC MCL gain
        ezca['IMC-MCL_TRAMP'] = 0
        ezca['IMC-MCL_GAIN'] = -125
        ezca['IMC-MCL_TRAMP'] = 7

    def run(self):
        # wait for suspensions to become aligned
        for sus in ['MC1', 'MC2', 'MC3']:
            if nodes['SUS_'+sus] != 'ALIGNED':
                return
        if in_fault():
            return 'FAULT'
        return True

        # if self.timer['conf']:
          #  return True

# turn on acquire feedback, and wait for lock
class ACQUIRE(GuardState):
    request = False

    def main(self):
        # engage feedback to servo and MC2
        ezca['IMC-REFL_SERVO_IN1EN'] = 'Off'
        ezca['IMC-REFL_SERVO_FASTEN'] = 1
        ezca.switch('IMC-L', 'OUTPUT', 'ON')
        ezca.switch('SUS-MC2_M2_LOCK_L', 'OUTPUT', 'ON')
        ezca['SUS-MC2_M2_LOCK_OUTSW_L'] = 'ON'
        ezca.switch('SUS-MC2_M3_LOCK_L', 'OUTPUT', 'ON')
        ezca['SUS-MC2_M3_LOCK_OUTSW_L'] = 'ON'
        ezca['IMC-L_GAIN'] = 1
        ezca['IMC-REFL_SERVO_IN1EN'] = 'On'
        # engage slow path filters
        # ezca.switch('IMC-MCL', 'FM1', 'FM2', 'ON')

        # turn off trigger PD low-pass during lock
        # filter is empty anyways
        # ezca.switch('IMC-MC2_TRANS_SUM', 'FM1', 'OFF')

        # turn on filter triggering
        #ezca['IMC-MCL_MASK_FM2'] = 1
        self.timer['too_long_to_acquire'] = 15 #30

    def run(self):
        if in_fault():
            return 'FAULT'        

        log('waiting for lock...')
        if is_locked():
            return True
        
        if self.timer['too_long_to_acquire']:
            return 'ACQUIRE'

# pause to settle and confirm we're in a somewhat
# stable lock
class LOCK_CONFIRM(GuardState):
    request = False

    def main(self):
        self.timer['conf'] = 1

    def run(self):
        # notify on faults
        in_fault()

        # if lock not confirmed, drop back to ACQUIRE
        if not is_locked():
            return 'ACQUIRE'

        if self.timer['conf']:
            return True

# turn on boosts and ramp up servo input gain
class LOCK_UP(GuardState):
    request = False

    def main(self):
        # turn on m2 lock output and boost
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0.1
        ezca.switch('SUS-MC2_M2_LOCK_L', 'FM3', 'ON')

        # turn off filter triggering
        # FIXME: why do we turn thesee off once lock is acquired?
        #ezca['IMC-MCL_MASK_FM2'] = 'OFF'

        # turn back off trigger PD low-pass
        #ezca.switch('IMC-MC2_TRANS_SUM', 'FM1', 'ON')
        # Old reference (before 09/16/14): The gain of -2dB is good for 3W input so use that as a reference
        # New reference: Set gain of 15dB for 1W -- IMC UGF = 62kHz
        self.comm_gain_set = 15 + 20*math.log(1.0/ezca['IMC-PWR_IN_OUTPUT'],10)
#        self.comm_gain_set = 13 + 20*math.log(1.0/ezca['IMC-PWR_IN_OUTPUT'],10)


    def run(self):
        # notify on faults
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        # ramp up the servo gain from its initial value to 0.
        # guardian runs at 32 Hz, so this should take 1 second.
        # complete state when it reaches desired gain
        # FIXME: this should be improved with a better built-in
        # guardian method
        if ezca['IMC-REFL_SERVO_IN1GAIN'] < self.comm_gain_set:
            ezca['IMC-REFL_SERVO_IN1GAIN'] += 1
            return

        ezca['IMC-REFL_SERVO_IN1GAIN'] = self.comm_gain_set
        return True

# turn on the last few filters and gains, then check for lock loss
class LOCK_UP2(GuardState):
    request = False

    def main(self):
        # engage the servo comboost
        ezca['IMC-REFL_SERVO_COMBOOST'] = 1

        # engage M1 path
        ezca['SUS-MC2_M1_LOCK_L_RSET'] = 2
        ezca.switch('SUS-MC2_M1_LOCK_L', 'FM2', 'FM1', 'INPUT', 'ON')
        ezca['SUS-MC2_M1_LOCK_L_TRAMP'] = 5
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = 1
        # timer to wait for gain ramp
        # self.timer['tramp'] = 3

        # Set WFS limits to 400
        ezca['IMC-DOF_1_P_LIMIT'] = 400
        ezca['IMC-DOF_1_Y_LIMIT'] = 400
        ezca['IMC-DOF_2_P_LIMIT'] = 400
        ezca['IMC-DOF_2_Y_LIMIT'] = 400
        ezca['IMC-DOF_3_P_LIMIT'] = 400
        ezca['IMC-DOF_3_Y_LIMIT'] = 400
        ezca['IMC-DOF_4_P_LIMIT'] = 400
        ezca['IMC-DOF_4_Y_LIMIT'] = 400

        # Turn on the WFS limits
        ezca.switch('IMC-DOF_1_P', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_1_Y', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_2_P', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_2_Y', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_3_P', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_3_Y', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_4_P', 'LIMIT', 'ON')
        ezca.switch('IMC-DOF_4_Y', 'LIMIT', 'ON')

        # Turn on the WFS loops and MC2 trans loop
        ezca.switch('IMC-DOF_1_P', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_1_Y', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_2_P', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_2_Y', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_4_P', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_4_Y', 'INPUT', 'ON')

        # Turn on the PZT resonance damping loop
        ezca.switch('IMC-DOF_5_P', 'INPUT', 'ON')
        # ezca.switch('IMC-DOF_5_Y', 'INPUT', 'ON')  # not used, uncomment if needed


        # These are the timer and flag pertaining to the wfs_dc loops 
        self.timer['wfs_dc'] = 30

    def run(self):
        # notify on faults
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        # wait for gain ramp before turning boost
        #if self.timer['tramp']:
        #ezca.switch('SUS-MC2_M1_LOCK_L', 'FM1', 'ON')

        # Waiting for 180s to allow all other loops to settle before 
        # turning on the DC spot centering:
        if not self.timer['wfs_dc']:
            return
        ezca.switch('IMC-DOF_3_P', 'INPUT', 'ON')
        ezca.switch('IMC-DOF_3_Y', 'INPUT', 'ON')

        # turn on the ISS second loop
        ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 32000

        return True

class LOCKED(GuardState):
    def run(self):
        # notify on faults
        in_fault()

        if not is_locked():
            log('lock loss, returning to down...')
            return 'DOWN'

        return True


##################################################

edges = [
    ('DOWN', 'ACQUIRE'),
    ('ACQUIRE', 'LOCK_CONFIRM'),
    ('LOCK_CONFIRM', 'LOCK_UP'),
    ('LOCK_UP', 'LOCK_UP2'),
    ('LOCK_UP2', 'LOCKED'),
    ]

##################################################
# SVN $Id: IFO_IMC.py 10545 2015-05-08 00:17:35Z adam.mullavey@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sys/l1/guardian/IFO_IMC.py $
##################################################
